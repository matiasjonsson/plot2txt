package com.plot2txt.p2t;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Parcel;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.*;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class StartProgramActivity extends AppCompatActivity {

    public final static String IMAGE_SELECTED = "imageSelected";
    public final static String IMAGE_URI = "imageUriJustInCase";
    public final static String GOING_DOWN = "goingDown";
    public final static String API_SELECTED = "apiSelected";
    public final static String TEMPLATE_SELECTED = "templateSelected";
    public final static String TEMPLATE_URI = "templateUriJustInCase";
    public final static String DIRECTORY = "directoryOfOutputs";
    public final static String URL_ENCODED_TEMPLATE = "urlEncodedTemplate";
    public final static String URIS_FROM_OUTPUT = "UrisFromOutput";
    public final static String FILES_FROM_OUTPUT = "filesFromOutput";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_program);
        getSupportActionBar().setTitle("plot2txt BETA");
        overridePendingTransition(R.anim.slide3, R.anim.slide4);
    }


    public void onStartPressed(View view) {
        showPhoneStatePermission();
    }

    public void startSelectAPI() {
        Intent intent = new Intent(this, SelectAPIActivity.class);
        intent.putExtra(GOING_DOWN, true);
        startActivity(intent);
    }

    public void onAboutPressed(View view){
        Intent intent = new Intent(this, AboutActivity.class);
        intent.putExtra(GOING_DOWN, true);
        startActivity(intent);
    }
    public void onHelpPressed(View view){
        Intent intent = new Intent(this, HelpActivity.class);
        intent.putExtra(GOING_DOWN, true);
        startActivity(intent);
    }










    private final int REQUEST_PERMISSION_PHONE_STATE=1;
    public void showPhoneStatePermission() {

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        System.out.println(permissionCheck+" "+PackageManager.PERMISSION_GRANTED);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            //if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showExplanation("This app needs permissions to read and write to the phone's storage.", "This is to allow the app to send and receive files from Plot2txt servers.", Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_PERMISSION_PHONE_STATE);
            //}
            //else {
            //    requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, REQUEST_PERMISSION_PHONE_STATE);
            //}

        }
        else {
            startSelectAPI();
        }                                                                                        //HttpPost Request starts here!
    }
    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            String permissions[],
            int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_PHONE_STATE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(StartProgramActivity.this, "Permission Granted!", Toast.LENGTH_SHORT).show();
                    startSelectAPI();                                                                                        //HttpPost Request starts here!
                } else {
                    Toast.makeText(StartProgramActivity.this, "Permission Denied! App cannot run without permission.", Toast.LENGTH_SHORT).show();
                }
        }
    }
    private void showExplanation(String title,
                                 String message,
                                 final String permission,
                                 final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission(permission, permissionRequestCode);
                    }
                });
        builder.create().show();
    }
    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionName}, permissionRequestCode);
    }

}

