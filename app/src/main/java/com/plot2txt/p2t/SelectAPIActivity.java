package com.plot2txt.p2t;

import android.content.*;
import android.support.v4.app.*;
import android.support.v7.app.*;
import android.os.Bundle;
import android.view.*;
import android.view.View;



public class SelectAPIActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_api);
        getSupportActionBar().setTitle("Select tool category");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(getIntent().getBooleanExtra(StartProgramActivity.GOING_DOWN, false))
            overridePendingTransition(R.anim.slide1, R.anim.slide2);
        else
            overridePendingTransition(R.anim.slide3, R.anim.slide4);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // Respond to the action bar's Up/Home button
            case R.id.home: {
                /*Intent intent =  new Intent(this, FileChooserActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra(StartProgramActivity.GOING_DOWN, false);
                NavUtils.navigateUpTo(this, intent);
                */
                onBackPressed();
                return true;
            }
        }
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide3,R.anim.slide4);
    }


    public void onScatterPressed(View view) {
        Intent getStarted = new Intent(this, FileChooserActivity.class);
        getStarted.putExtra(StartProgramActivity.GOING_DOWN, true);
        getStarted.putExtra(StartProgramActivity.API_SELECTED,21);
        //getStarted.putExtra(StartProgramActivity.IMAGE_SELECTED, getIntent().getStringExtra(StartProgramActivity.IMAGE_SELECTED));
        //getStarted.putExtra(StartProgramActivity.IMAGE_URI, getIntent().getStringExtra(StartProgramActivity.IMAGE_URI));
        startActivity(getStarted);
    }
    public void onLinePressed(View view) {
        Intent getStarted = new Intent(this, FileChooserActivity.class);
        getStarted.putExtra(StartProgramActivity.GOING_DOWN, true);
        getStarted.putExtra(StartProgramActivity.API_SELECTED,22);
        //getStarted.putExtra(StartProgramActivity.IMAGE_SELECTED, getIntent().getStringExtra(StartProgramActivity.IMAGE_SELECTED));
        //getStarted.putExtra(StartProgramActivity.IMAGE_URI, getIntent().getStringExtra(StartProgramActivity.IMAGE_URI));
        startActivity(getStarted);
    }

}
