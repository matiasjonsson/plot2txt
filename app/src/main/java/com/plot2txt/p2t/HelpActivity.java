package com.plot2txt.p2t;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        getSupportActionBar().setTitle("Help");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(getIntent().getBooleanExtra(StartProgramActivity.GOING_DOWN, false))
            overridePendingTransition(R.anim.slide1, R.anim.slide2);
        else
            overridePendingTransition(R.anim.slide3, R.anim.slide4);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // Respond to the action bar's Up/Home button
            case R.id.home: {
                onBackPressed();
                return true;
            }
        }
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide3,R.anim.slide4);
    }

    public void onAboutPressed(View view){
        Intent intent = new Intent(this, AboutActivity.class);
        intent.putExtra(StartProgramActivity.GOING_DOWN, true);
        startActivity(intent);
    }
    public void contactUs(View view){
        Intent send = new Intent(Intent.ACTION_SENDTO);
        String uriText = "mailto:" + Uri.encode("wjb@plot2txt.com") +
                "?subject=" + Uri.encode("plot2txt android app help") +
                "&body=" + Uri.encode("");
        Uri uri = Uri.parse(uriText);

        send.setData(uri);
        startActivity(Intent.createChooser(send, "Send email..."));
    }

    public void visitOurWebsite(View view){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.plot2txt.com"));
        startActivity(browserIntent);
    }

}