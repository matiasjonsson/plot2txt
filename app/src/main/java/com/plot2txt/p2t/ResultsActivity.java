package com.plot2txt.p2t;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Space;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.support.v4.content.FileProvider;

import javax.xml.transform.Result;

public class ResultsActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private int apiSelected;
    private boolean isFABOpen = false;
    protected FloatingActionButton fab1;
    protected FloatingActionButton fab2;
    protected FloatingActionButton fab3;
    protected FloatingActionButton fab4;
    protected FloatingActionButton fabMenu;
    private boolean saved=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        apiSelected = getIntent().getIntExtra(StartProgramActivity.API_SELECTED,0);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Results from "+apiSelected()+" API");



        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),
                apiSelected,
                ResultsActivity.this,
                getIntent());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));


        if(getIntent().getBooleanExtra(StartProgramActivity.GOING_DOWN, false))
            overridePendingTransition(R.anim.slide1, R.anim.slide2);
        else
            overridePendingTransition(R.anim.slide3, R.anim.slide4);


        fab1 = (FloatingActionButton)findViewById(R.id.fab);
        fab2 = (FloatingActionButton)findViewById(R.id.fab2);
        fab3 = (FloatingActionButton)findViewById(R.id.fab3);
        fab4 = (FloatingActionButton)findViewById(R.id.fab4);
        fabMenu = (FloatingActionButton)findViewById(R.id.fab_expand);
    }



    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item= menu.findItem(R.id.action_settings);
        item.setVisible(false);
        super.onPrepareOptionsMenu(menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // Respond to the action bar's Up/Home button
            case R.id.home: {
                /*Intent intent = new Intent(this, FileChooserActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(StartProgramActivity.GOING_DOWN,false);
                startActivity(intent);*/
                onBackPressed();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    boolean pressedTwice = false;
    @Override
    public void onBackPressed() {
        if(!isFABOpen&&!pressedTwice){
            Snackbar.make((CoordinatorLayout)findViewById(R.id.main_content_results),"Going back will delete the processed files if you do not save them.",Snackbar.LENGTH_LONG)
                    .setAction("Go back", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(ResultsActivity.this, FileChooserActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra(StartProgramActivity.GOING_DOWN,false);
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide3,R.anim.slide4);
                        }
                    }).addCallback(new Snackbar.Callback(){
                @Override
                public void onDismissed(Snackbar snackbar, int event) {
                    pressedTwice=false;
                }

                @Override
                public void onShown(Snackbar snackbar) {
                    pressedTwice=true;
                }
            }).show();

        }
        else if(pressedTwice){
            Intent intent = new Intent(ResultsActivity.this, FileChooserActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra(StartProgramActivity.GOING_DOWN,false);
            startActivity(intent);
            overridePendingTransition(R.anim.slide3,R.anim.slide4);
        }
        else{
            closeFABMenu();
        }
    }
    @Override
    public void onDestroy() {
        if(saved)
            deleteFiles();
        super.onDestroy();
    }
    public String apiSelected() {
        if(apiSelected==21) {return "Scatter"; }
        else if(apiSelected==22) { return "Line"; }
        else {
            Toast.makeText(this,"Sorry, there was an error on our end! Please try again. If this continues occurring, shoot us an email.", Toast.LENGTH_LONG).show();
            onBackPressed();
            return null;
        }
    }

    public void onMenuPressed(View view){
        if(!isFABOpen){
            showFABMenu();
        }else{
            closeFABMenu();
        }
    }
    private void showFABMenu(){
        isFABOpen=true;
        fab1.animate().translationY(-getResources().getDimension(R.dimen.standard_76));
        fab2.animate().translationY(-getResources().getDimension(R.dimen.standard_142));
        fab3.animate().translationY(-getResources().getDimension(R.dimen.standard_208));
        fab4.animate().translationY(-getResources().getDimension(R.dimen.standard_274));
        fabMenu.setImageResource(R.drawable.close);
    }
    private void closeFABMenu(){
        isFABOpen=false;
        fab1.animate().translationY(0);
        fab2.animate().translationY(0);
        fab3.animate().translationY(0);
        fab4.animate().translationY(0);
        fabMenu.setImageResource(R.drawable.content_save);

    }
    public void onDownloadPressed(View view){
        File photo = new File(getIntent().getStringExtra(StartProgramActivity.IMAGE_SELECTED));
        String timeStamp = new SimpleDateFormat("yyyy_MMdd_HHmmss").format(new Date());
        File downloads = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),photo.getName()+timeStamp);
        String[] files = getIntent().getStringArrayExtra(StartProgramActivity.FILES_FROM_OUTPUT);
        try {
            FileUtils.copyFile(photo, new File(downloads, photo.getName()));
            for(int count = 0; count<files.length;count++)
                FileUtils.copyFile(new File(files[count]), new File(downloads, new File(files[count]).getName()));
            Snackbar.make(view, "Successfully saved to Downloads folder!", Snackbar.LENGTH_LONG).show();

        }catch(IOException e){
            Snackbar.make(view, "Error saving files. Try again, or shoot us an email if it keeps happening.", Snackbar.LENGTH_LONG);
            System.out.println(e.toString());
        }
        closeFABMenu();
        saved=true;
    }
    public void onDrivePressed(View view){
        String[] files = getIntent().getStringArrayExtra(StartProgramActivity.FILES_FROM_OUTPUT);
        System.out.println(files[0]);
        ArrayList<Uri> uris = new ArrayList<Uri>();
        File temp;
        Uri temp2;
        uris.add(FileProvider.getUriForFile(getApplicationContext(), "com.plot2txt.p2t.fileprovider", new File(getIntent().getStringExtra(StartProgramActivity.IMAGE_SELECTED))));

        for(int count=0; count<files.length;count++){
            temp = new File(files[count]);
            temp2 = FileProvider.getUriForFile(getApplicationContext(), "com.plot2txt.p2t.fileprovider",temp.getAbsoluteFile());
            uris.add(temp2);
        }

        Intent uploadIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        uploadIntent.setType("vnd.android.cursor.dir/drive");
        uploadIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM,uris);
        uploadIntent.putExtra(Intent.EXTRA_SUBJECT, "plot2txt Output");
        startActivity(Intent.createChooser(uploadIntent, "Upload your files to Google Drive"));
        saved=true;
    }
    public void onEmailPressed(View view) {
        String[] files = getIntent().getStringArrayExtra(StartProgramActivity.FILES_FROM_OUTPUT);
        System.out.println(files[0]);
        ArrayList<Uri> uris = new ArrayList<Uri>();
        File temp;
        Uri temp2;
        uris.add(FileProvider.getUriForFile(getApplicationContext(), "com.plot2txt.p2t.fileprovider", new File(getIntent().getStringExtra(StartProgramActivity.IMAGE_SELECTED))));

        for(int count=0; count<files.length;count++){
            temp = new File(files[count]);
            temp2 = FileProvider.getUriForFile(getApplicationContext(), "com.plot2txt.p2t.fileprovider",temp.getAbsoluteFile());
            uris.add(temp2);
        }

        Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        emailIntent.setType("vnd.android.cursor.dir/email");
        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM,uris);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "plot2txt Output");
        startActivity(Intent.createChooser(emailIntent , "Send yourself the outputs as email attachments!"));
        saved=true;
    }
    public void onDeletePressed(final View view){
        Snackbar.make(view, "Are you sure you want to delete the results and start over?", Snackbar.LENGTH_LONG)
                .setAction("Delete", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        File directory = new File(getIntent().getStringExtra(StartProgramActivity.DIRECTORY));
                        try {
                            FileUtils.deleteQuietly(directory);
                        }
                        catch (Exception e) {
                            Snackbar.make(view, "Unable to delete files! Shoot us an email if this keeps happening.", Snackbar.LENGTH_LONG);
                            System.out.println(e.toString());
                        }
                        Intent intent =  new Intent(ResultsActivity.this, SelectAPIActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.putExtra(StartProgramActivity.GOING_DOWN, false);
                        NavUtils.navigateUpTo(ResultsActivity.this, intent);
                    }
                }).show();
    }


    public void deleteFiles(){
        File directory = new File(getIntent().getStringExtra(StartProgramActivity.DIRECTORY));
        try {
            FileUtils.deleteQuietly(directory);
        }
        catch (Exception e) {
            //Snackbar.make((CoordinatorLayout)findViewById(R.id.main_content_results), "Unable to delete files! Shoot us an email if this keeps happening.", Snackbar.LENGTH_LONG);
            System.out.println(e.toString());
        }
    }















    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_results, menu);
        return true;
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static int apiSelected;
        public static Context context;
        public static Intent intent;
        public static PlaceholderFragment newInstance(int sectionNumber,int api,Context c,Intent i) {
            context = c;
            apiSelected = api;
            intent = i;
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView;
            if(getArguments().getInt(ARG_SECTION_NUMBER)==1){
                rootView = inflater.inflate(R.layout.fragment_results, container, false);
                displayTable(rootView);
            }
            else{
                rootView = inflater.inflate(R.layout.fragment_results_images, container, false);
                displayImages(rootView);
            }
            return rootView;
        }

        public void displayTable(View view) {
            ArrayList<Uri> files = intent.getParcelableArrayListExtra(StartProgramActivity.URIS_FROM_OUTPUT);
            String fileLocationString;
            int json = -1;
            int exit = 0;
            switch(exit) {
                case 0:

                    for (int count = 0; count < files.size(); count++) {
                        fileLocationString = files.get(count).getPath();
                        if (fileLocationString.substring(fileLocationString.lastIndexOf('.'), fileLocationString.length()).equals(".json")) {
                            json = count;
                            count = files.size();
                        }
                    }


                    if (json == -1) {
                        Toast.makeText(context, "Sorry, there was an error on our end! Please try again. If this continues occurring, shoot us an email.", Toast.LENGTH_LONG).show();
                        break;
                    }


                    String jsonArrayString = "";
                    try {
                        BufferedReader br = new BufferedReader(new FileReader(files.get(json).getPath()));
                        String st;
                        while ((st = br.readLine()) != null)
                            jsonArrayString += st;
                        if (jsonArrayString.length() == 0)
                            throw new Exception("json file was empty");
                    } catch (Exception e) {
                        Toast.makeText(context, "Sorry, there was an error on our end! Please try again. If this continues occurring, shoot us an email.", Toast.LENGTH_LONG).show();
                        System.out.println(e.toString());
                        break;
                    }


                    if (!(jsonArrayString.charAt(0) == '[') || !(jsonArrayString.charAt(1) == '{')) {
                        TextView t = (TextView) view.findViewById(R.id.textViewError);
                        String errorString = "Error Message:\n" + jsonArrayString + "\nPlease try again or try a new image.";
                        t.setText(errorString);
                        t.setVisibility(View.VISIBLE);
                        break;
                    } else {
                        switch (apiSelected) {
                            case 21:
                            case 22:


                                try {
                                    JSONArray jArray = new JSONArray(jsonArrayString);

                                    TableLayout tv = (TableLayout) view.findViewById(R.id.tableLayout1);
                                    tv.removeAllViewsInLayout();
                                    int flag = 1;

                                    // when i=-1, loop will display heading of each column
                                    // then usually data will be display from i=0 to jArray.length()
                                    for (int i = -1; i < jArray.length(); i++) {

                                        TableRow tr = new TableRow(context);

                                        tr.setLayoutParams(new ViewGroup.LayoutParams(
                                                ViewGroup.LayoutParams.MATCH_PARENT,
                                                ViewGroup.LayoutParams.WRAP_CONTENT));
                                        // this will be executed once
                                        if (flag == 1) {


                                            TextView b3 = new TextView(context);
                                            b3.setText("x");
                                            b3.setTypeface(Typeface.create("roboto",Typeface.NORMAL));
                                            b3.setGravity(Gravity.CENTER);
                                            tr.addView(b3);

                                            TextView b5 = new TextView(context);
                                            b5.setText("|");
                                            b5.setTypeface(Typeface.create("roboto",Typeface.NORMAL));
                                            b5.setGravity(Gravity.CENTER);
                                            tr.addView(b5);

                                            TextView b4 = new TextView(context);
                                            b4.setText("y");
                                            b4.setTypeface(Typeface.create("roboto",Typeface.NORMAL));
                                            b4.setGravity(Gravity.CENTER);
                                            tr.addView(b4);


                                            tv.addView(tr);

                                            final View vline = new View(context);
                                            vline.setLayoutParams(new
                                                    TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                                            vline.setBackgroundColor(Color.BLUE);
                                            tv.addView(vline); // add line below heading
                                            flag = 0;
                                        } else {
                                            JSONObject json_data = jArray.getJSONObject(i);

                                            TextView b = new TextView(context);
                                            String str = String.valueOf(json_data.getDouble("x"));
                                            b.setTypeface(Typeface.create("roboto",Typeface.NORMAL));
                                            b.setText(str);
                                            b.setGravity(Gravity.CENTER);
                                            tr.addView(b);

                                            TextView b2 = new TextView(context);
                                            b2.setTypeface(Typeface.create("roboto",Typeface.NORMAL));
                                            String str2 = "|";
                                            b2.setText(str2);
                                            b2.setGravity(Gravity.CENTER);
                                            tr.addView(b2);

                                            TextView b1 = new TextView(context);
                                            b1.setTypeface(Typeface.create("roboto",Typeface.NORMAL));
                                            String str1 = String.valueOf(json_data.getDouble("y"));
                                            b1.setText(str1);
                                            b1.setGravity(Gravity.CENTER);
                                            tr.addView(b1);

                                            tv.addView(tr);
                                            final View vline1 = new View(context);
                                            vline1.setLayoutParams(new
                                                    TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                            vline1.setBackgroundColor(Color.WHITE);
                                            tv.addView(vline1);  // add line below each row


                                        }

                                    }
                                    tv.setColumnStretchable(2, true);
                                    tv.setColumnStretchable(0, true);

                                } catch (org.json.JSONException e) {
                                    System.out.println("Error parsing data " + e.toString());
                                    Toast.makeText(getActivity().getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
                                }

                        }

                    }
            }
        }
        public void displayImages(View view) {
            ArrayList<Uri> files = intent.getParcelableArrayListExtra(StartProgramActivity.URIS_FROM_OUTPUT);
            String fileLocationString;
            int json = -1;
            int exit = 0;
            switch(exit) {
                case 0:

                    for (int count = 0; count < files.size(); count++) {
                        fileLocationString = files.get(count).getPath();
                        if (fileLocationString.substring(fileLocationString.lastIndexOf('.'), fileLocationString.length()).equals(".json")) {
                            json = count;
                            count = files.size();
                        }
                    }


                    if (json == -1) {
                        Toast.makeText(context, "Sorry, there was an error on our end! Please try again. If this continues occurring, shoot us an email.", Toast.LENGTH_LONG).show();
                        break;
                    }




                    File orig=null;
                    try {
                        orig = new File(intent.getStringExtra(StartProgramActivity.IMAGE_SELECTED));
                        ImageView origImageView = (ImageView) view.findViewById(R.id.imageViewOrig);
                        origImageView.setImageURI(Uri.fromFile(orig));

                    }
                    catch (NullPointerException n) {
                        Toast.makeText(context, "Sorry, there was an error on our end! Please try again. If this continues occurring, shoot us an email.", Toast.LENGTH_LONG).show();
                        System.out.println(n.toString());
                        break;
                    }
                    if(orig==null)
                        break;

                    LinearLayout linearLayout = (LinearLayout)view.findViewById(R.id.linearLayoutForImageResults);
                    int eightDP = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, context.getResources().getDisplayMetrics());
                    int twentyDP = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, context.getResources().getDisplayMetrics());
                    int textsize = 30;
                    int num = 1;
                    for(int count = 0; count<files.size();count++){
                        if(count==json)
                            count++;
                        if(count<files.size()) {
                            Uri currentImage = files.get(count);

                            CardView c = new CardView(context);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(CardView.LayoutParams.MATCH_PARENT, CardView.LayoutParams.WRAP_CONTENT);
                            params.setMargins(eightDP,0,eightDP,eightDP);
                            c.setLayoutParams(params);
                            c.setElevation(eightDP);


                            LinearLayout insideCardView = new LinearLayout(context);
                            CardView.LayoutParams params2 = new CardView.LayoutParams(CardView.LayoutParams.MATCH_PARENT,CardView.LayoutParams.WRAP_CONTENT);
                            insideCardView.setOrientation(LinearLayout.VERTICAL);
                            insideCardView.setLayoutParams(params2);

                            ImageView imageView = new ImageView(context);
                            LinearLayout.LayoutParams params9 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                            params9.setMargins(twentyDP,twentyDP,twentyDP,0);
                            imageView.setImageURI(currentImage);
                            imageView.setLayoutParams(params9);
                            imageView.setAdjustViewBounds(true);

                            insideCardView.addView(imageView);

                            Space space = new Space(context);
                            CardView.LayoutParams params3 = new CardView.LayoutParams(CardView.LayoutParams.MATCH_PARENT,twentyDP);
                            space.setLayoutParams(params3);

                            insideCardView.addView(space);

                            TextView textView = new TextView(context);
                            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                            params4.setMargins(twentyDP,0,twentyDP,twentyDP);
                            textView.setTextSize(textsize);
                            textView.setTypeface(Typeface.create("roboto",Typeface.NORMAL));
                            textView.setTextColor(getResources().getColor(R.color.dark));
                            textView.setText("Output image #"+num);
                            num++;

                            insideCardView.addView(textView);


                            c.addView(insideCardView);
                            linearLayout.addView(c);
                            textView.setLayoutParams(params4);

                        }
                    }



/*
                    String jsonArrayString = "";
                    try {
                        BufferedReader br = new BufferedReader(new FileReader(files.get(json).getPath()));
                        String st;
                        while ((st = br.readLine()) != null)
                            jsonArrayString += st;
                        if (jsonArrayString.length() == 0)
                            throw new Exception("json file was empty");
                    } catch (Exception e) {
                        Toast.makeText(context, "Sorry, there was an error on our end! Please try again. If this continues occurring, shoot us an email.", Toast.LENGTH_LONG).show();
                        System.out.println(e.toString());
                        break;
                    }


                    if ((jsonArrayString.charAt(0) == '[') || !(jsonArrayString.charAt(1) == '{')) {
                        TextView t = (TextView) view.findViewById(R.id.textViewError);
                        String errorString = "Error Message:\n" + jsonArrayString + "\nPlease try again or try a new image.";
                        t.setText(errorString);
                        t.setVisibility(View.VISIBLE);
                        break;
                    } else {
                        switch (apiSelected) {
                            case 21:
                            case 22:


                                try {

                                    System.out.println(jsonArrayString.substring(0, 100));
                                    JSONArray jArray = new JSONArray(jsonArrayString);

                                    TableLayout tv = (TableLayout) view.findViewById(R.id.tableLayout1);
                                    tv.removeAllViewsInLayout();
                                    int flag = 1;

                                    // when i=-1, loop will display heading of each column
                                    // then usually data will be display from i=0 to jArray.length()
                                    for (int i = -1; i < jArray.length(); i++) {

                                        TableRow tr = new TableRow(context);

                                        tr.setLayoutParams(new ViewGroup.LayoutParams(
                                                ViewGroup.LayoutParams.MATCH_PARENT,
                                                ViewGroup.LayoutParams.WRAP_CONTENT));
                                        // this will be executed once
                                        if (flag == 1) {


                                            TextView b3 = new TextView(context);
                                            b3.setText("x");
                                            b3.setGravity(Gravity.CENTER);
                                            tr.addView(b3);

                                            TextView b5 = new TextView(context);
                                            b5.setText("|");
                                            b5.setGravity(Gravity.CENTER);
                                            tr.addView(b5);

                                            TextView b4 = new TextView(context);
                                            b4.setText("y");
                                            b4.setGravity(Gravity.CENTER);
                                            tr.addView(b4);


                                            tv.addView(tr);

                                            final View vline = new View(context);
                                            vline.setLayoutParams(new
                                                    TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 2));
                                            vline.setBackgroundColor(Color.BLUE);
                                            tv.addView(vline); // add line below heading
                                            flag = 0;
                                        } else {
                                            JSONObject json_data = jArray.getJSONObject(i);

                                            TextView b = new TextView(context);
                                            String str = String.valueOf(json_data.getDouble("x"));
                                            b.setText(str);
                                            b.setGravity(Gravity.CENTER);
                                            tr.addView(b);

                                            TextView b2 = new TextView(context);
                                            String str2 = "|";
                                            b2.setText(str2);
                                            b2.setGravity(Gravity.CENTER);
                                            tr.addView(b2);

                                            TextView b1 = new TextView(context);
                                            String str1 = String.valueOf(json_data.getDouble("y"));
                                            b1.setText(str1);
                                            b1.setGravity(Gravity.CENTER);
                                            tr.addView(b1);

                                            tv.addView(tr);
                                            final View vline1 = new View(context);
                                            vline1.setLayoutParams(new
                                                    TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, 1));
                                            vline1.setBackgroundColor(Color.WHITE);
                                            tv.addView(vline1);  // add line below each row


                                        }

                                    }
                                    tv.setColumnStretchable(2, true);
                                    tv.setColumnStretchable(0, true);

                                } catch (org.json.JSONException e) {
                                    System.out.println("Error parsing data " + e.toString());
                                    Toast.makeText(getActivity().getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
                                }

                        }

                    }*/
            }
        }




    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        int apiSelected;
        Context context;
        Intent intent;
        public SectionsPagerAdapter(FragmentManager fm, int api,Context c,Intent i) {
            super(fm);
            apiSelected=api;
            context = c;
            intent = i;

        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1,apiSelected,context,intent);
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }
    }
}
