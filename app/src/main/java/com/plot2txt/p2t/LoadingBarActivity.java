package com.plot2txt.p2t;


import android.content.*;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.*;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.*;
import org.rauschig.jarchivelib.*;

import java.io.*;
import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class LoadingBarActivity extends AppCompatActivity {
    HttpPost httpPost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_bar);
        getSupportActionBar().setTitle("Processing request");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(getIntent().getBooleanExtra(StartProgramActivity.GOING_DOWN, false))
            overridePendingTransition(R.anim.slide1, R.anim.slide2);
        else
            overridePendingTransition(R.anim.slide3, R.anim.slide4);
        httpPost = new HttpPost();
        httpPost.execute();


    }
    private void galleryAddPic(File file) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(file));
        this.sendBroadcast(mediaScanIntent);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // Respond to the action bar's Up/Home button
            case R.id.home: {
                httpPost.cancel(true);
                /*Intent intent =  new Intent(this, FileChooserActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra(StartProgramActivity.GOING_DOWN, false);
                NavUtils.navigateUpTo(this, intent);
                */
                onBackPressed();
                return true;
            }
        }
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide3,R.anim.slide4);
    }


    @Override
    public void onDestroy() {
        httpPost.cancel(true);
        super.onDestroy();
    }






    class HttpPost extends AsyncTask<Void, Void, Void> {

        protected File image,archive,unpacked;
        protected String requestType,url,parameters;
        protected int apiSelected;
        protected Bitmap bitmap;
        @Override
        protected Void doInBackground(Void... params) {
            try {
                try {
                    image = new File(getIntent().getStringExtra(StartProgramActivity.IMAGE_SELECTED));
                    System.out.println("NOT ADDED");
                }
                catch (NullPointerException n) {
                    image = createImageFromUri(Uri.parse(getIntent().getStringExtra(StartProgramActivity.IMAGE_URI)));
                    galleryAddPic(image);
                    System.out.println("ADDED");
                }
                System.out.println(image.getAbsolutePath());
                setupURL();
                archive = new File(LoadingBarActivity.this.getExternalFilesDir(null),image.getName().substring(0, image.getName().length()-4)+"-"+requestType+".tar.gz");
                HttpURLConnection linkToServer = setupPostRequest(url);
                sendFile(linkToServer);
                getResponse(linkToServer);
                unpackTGZ();

                displayResults();

            }
            catch (final Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoadingBarActivity.this,"Sorry, there was an error on our end! Please try again. If this continues occurring, shoot us an email.", Toast.LENGTH_LONG).show();
                        System.out.println(e.toString());
                        onBackPressed();

                    }
                });

                this.cancel(true);
            }

            return null;
        }

        protected int width,height,widthScaled,heightScaled;
        double factor;
        protected void setupURL() {
            apiSelected = getIntent().getIntExtra(StartProgramActivity.API_SELECTED,0);
            parameters = "";
            setupPNG();
            switch(apiSelected){
                case 0: onBackPressed(); break;
                case 1: {requestType = "group-colors"; } break;
                case 2: {requestType = "text-lines"; } break;
                case 3: {requestType = "degrid"; } break;
                case 4: {requestType = "blob-summary"; } break;
                case 5: {/*requestType = "gauss-mix";*/ onBackPressed(); } break;
                case 6: {requestType = "coords"; } break;
                case 7: {
                    requestType = "bbox";
                    if(getIntent().getIntExtra("maxArea",0) == 0)
                        onBackPressed();

                    if(factor == 1.0)
                        parameters = "/?upper_area=" + getIntent().getIntExtra("maxArea",0) +
                                      "&lower_area=" + getIntent().getIntExtra("minArea",0) +
                                        "&boundary=" + getIntent().getIntExtra("boundary",0);
                    else
                        parameters = "/?upper_area=" + (int)((double)getIntent().getIntExtra("maxArea",0) * factor * factor) +
                                      "&lower_area=" + (int)((double)getIntent().getIntExtra("minArea",0) * factor * factor)+
                                        "&boundary=" + (int)((double)getIntent().getIntExtra("boundary",0) * factor); } break;
                case 8: {
                    requestType = "binarize";
                    parameters = "/?red=" + getIntent().getIntExtra("red",240) +
                                "&green=" + getIntent().getIntExtra("green",240) +
                                 "&blue=" + getIntent().getIntExtra("blue",240); } break;
                case 9: {requestType = "pixel-freq"; } break;
                case 10: {
                    requestType = "pixel-sieve";
                    if(getIntent().getIntExtra("maxArea",0) == 0)
                        onBackPressed();

                    if(factor==1.0)
                        parameters = "/?hi_pixels=" + getIntent().getIntExtra("maxArea",0) +
                                      "&lo_pixels=" + getIntent().getIntExtra("minArea",0);
                    else
                        parameters = "/?hi_pixels=" + (int)((double)getIntent().getIntExtra("maxArea",0) * factor * factor) +
                                      "&lo_pixels=" + (int)((double)getIntent().getIntExtra("minArea",0) * factor * factor); } break;
                case 11: {/*requestType = "kernel";*/ onBackPressed(); } break;
                case 12: {/*requestType = "heat-map";*/ onBackPressed();} break;
                case 13: {requestType = "thinner"; } break;
                case 14: {requestType = "get-circles"; } break;
                case 15: {requestType = "get-lines"; } break;
                case 16: {requestType = "get-scale"; } break;
                case 17: {
                    requestType = "transform";
                    parameters = getIntent().getStringExtra("parameters");} break;
                case 18: {/*requestType = "graph";*/ onBackPressed();} break;
                case 19: {
                    requestType = "template-match";
                    parameters = "/?templ="+getIntent().getStringExtra(StartProgramActivity.URL_ENCODED_TEMPLATE); } break;
                case 20: {
                    requestType = "cut";
                    if(getIntent().getIntExtra("xmax",0) == 0 || getIntent().getIntExtra("ymax",0) == 0)
                        onBackPressed();

                    if(factor==1.0)
                        parameters = "/?xmn=" + getIntent().getIntExtra("xmin",0) +
                                      "&xmx=" + getIntent().getIntExtra("xmax",0) +
                                      "&ymn=" + getIntent().getIntExtra("ymin",0) +
                                      "&ymx=" + getIntent().getIntExtra("ymax",0);
                    else
                        parameters = "/?xmn=" + (int)((double)getIntent().getIntExtra("xmin",0) * factor) +
                                      "&xmx=" + (int)((double)getIntent().getIntExtra("xmax",0) * factor) +
                                      "&ymn=" + (int)((double)getIntent().getIntExtra("ymin",0) * factor) +
                                      "&ymx=" + (int)((double)getIntent().getIntExtra("ymax",0) * factor); } break;
                case 21: {
                    requestType = "simple-scatter-plot";
                    parameters = "/?opt=pixel_cm"; } break;
                case 22: {requestType = "simple-line-plot"; } break;
                default: {onBackPressed(); } break;
            }
            url = "https://b8jd85qv3b.execute-api.us-east-1.amazonaws.com/test/"+requestType+parameters;
        }
        protected void setupPNG() {
            try {
                Bitmap bitmapTemp = BitmapFactory.decodeFile(image.getAbsolutePath());
                width = bitmapTemp.getWidth();
                height = bitmapTemp.getHeight();
                if (width > 2048 || height > 2048) {
                    if (width > height) {
                        widthScaled = 2048;
                        heightScaled = (int) ((double) height * 2048.0 / (double) width);
                        factor = 2048.0 / (double) width;
                    } else if (height > width) {
                        heightScaled = 2048;
                        widthScaled = (int) ((double) width * 2048.0 / (double) height);

                        factor = 2048.0 / (double) height;
                    } else {
                        widthScaled = 2048;
                        heightScaled = 2048;
                        factor = 2048.0 / (double) width;
                    }
                    bitmap = Bitmap.createScaledBitmap(bitmapTemp, widthScaled, heightScaled, true);
                } else {
                    factor = 1.0;
                    bitmap = bitmapTemp;
                }
            }
            catch(final OutOfMemoryError e){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoadingBarActivity.this,"Sorry, your phone doesn't have enough memory (RAM) to process the image. Free up some memory and try again", Toast.LENGTH_LONG).show();
                        System.out.println(e.toString());
                        onBackPressed();

                    }
                });


            }
        }
        protected HttpURLConnection setupPostRequest(String url) throws IOException {
            URL whereThePostIsGoing = new URL(url);
            HttpURLConnection linkToServer = (HttpURLConnection) whereThePostIsGoing.openConnection();
            linkToServer.setRequestMethod("POST");
            linkToServer.setRequestProperty("Accept","image/png");
            linkToServer.setRequestProperty("Content-Type","image/png");
            linkToServer.setRequestProperty("Authorization","Token yA16oOZ9x3stH0ulSZYEdT0XKLTR02f1");
            linkToServer.setUseCaches(false);
            linkToServer.setDoInput(true);
            linkToServer.setDoOutput(true);
            linkToServer.setAllowUserInteraction(true);
            return linkToServer;
        }
        protected void sendFile(HttpURLConnection linkToServer) throws IOException {
            DataOutputStream sendFile = new DataOutputStream(linkToServer.getOutputStream());


            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] dataToSend = byteArrayOutputStream.toByteArray();

            //byte[] dataToSend = FileUtils.readFileToByteArray(image);

            sendFile.write(dataToSend);
            sendFile.flush();
            sendFile.close();
        }


        protected void getResponse(HttpURLConnection linkToServer) throws IOException {
            System.out.println("testing before responsecode");
            int responseCode = linkToServer.getResponseCode();
            System.out.println(responseCode);
            String responseMessage = linkToServer.getResponseMessage();
            System.out.println(responseCode+" "+responseMessage);
            DataInputStream response = new DataInputStream(linkToServer.getInputStream());
            byte[] responseData;

            if ((200 <= responseCode) && (responseCode < 300)) {
                responseData = new byte[linkToServer.getContentLength()];
                response.readFully(responseData);
                FileUtils.writeByteArrayToFile(archive, responseData);
            }
            else {
                throw new IOException(responseMessage + " (code: " + responseCode + ")");
            }

        }
        protected void unpackTGZ() throws IOException{
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            unpacked = new File(archive.getAbsolutePath().substring(0, archive.getPath().length()-7)+timeStamp);
            Archiver archiver = ArchiverFactory.createArchiver(ArchiveFormat.TAR, CompressionType.GZIP);
            archiver.extract(archive, unpacked);
            FileUtils.forceDelete(archive);
        }
        File[] files;
        protected ArrayList<Uri> getIndividualFilesFromUnpacked() {
            /*switch() {
                case "simple-line-plot": {
                    filesToEmail = new File[1];
                    filesToEmail[0] = new File(unpacked.getPath(), "/tmp/output.json");}
                    break;
                default: {filesToEmail = new File[]{image};}
                    break;
            }*/
            files = new File(unpacked, "/tmp").listFiles();
            ArrayList<Uri> filesToEmail = new ArrayList<Uri>();
            for(int x = 0; x < files.length; x++)
                filesToEmail.add(Uri.parse("file://" + files[x].getPath()));
            return filesToEmail;
        }
        protected File createImageFromUri(Uri uri) throws IOException{
            InputStream iStream = getContentResolver().openInputStream(uri);
            byte[] dataToSend = getBytes(iStream);
            File destination = createImageFile();
            FileUtils.writeByteArrayToFile(destination, dataToSend);
            return destination;
        }
        public byte[] getBytes(InputStream inputStream) throws IOException {
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];

            int len = 0;
            while ((len = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
            return byteBuffer.toByteArray();
        }
        private File createImageFile() throws IOException {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"/plot2txt/");
            if(!storageDir.exists())
                storageDir.mkdir();
            File imageTemp = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".png",         /* suffix */
                    storageDir      /* directory */
            );
            return imageTemp;
        }
        protected void displayResults() {
            final ArrayList<Uri> filesToSend = getIndividualFilesFromUnpacked();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Intent displayResults = new Intent(LoadingBarActivity.this, ResultsActivity.class);
                    String[] filesArrayList = new String[filesToSend.size()];
                    for(int x = 0; x<files.length; x++)
                        filesArrayList[x]=files[x].getAbsolutePath();
                    displayResults.putExtra(StartProgramActivity.URIS_FROM_OUTPUT,filesToSend);
                    displayResults.putExtra(StartProgramActivity.FILES_FROM_OUTPUT,filesArrayList);
                    displayResults.putExtra(StartProgramActivity.GOING_DOWN,true);
                    displayResults.putExtra(StartProgramActivity.IMAGE_SELECTED,image.getAbsolutePath());
                    displayResults.putExtra(StartProgramActivity.API_SELECTED,apiSelected);
                    displayResults.putExtra(StartProgramActivity.DIRECTORY,unpacked.getAbsolutePath());
                    startActivity(displayResults);
                }
            });




            //Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
            //emailIntent.setType("*/*");
            //emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, filesToSend);
            //emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Plot2Txt output");
            //startActivity(Intent.createChooser(emailIntent , "Send yourself the output as an email attachment! " +
            //        "(note: selecting the option \"Save to Drive\" will upload the body of the email to your Drive in an unreadable format, and, as such, is not advised)"));

        }

    }

}
