package com.plot2txt.p2t;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;


public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        //findViewById(android.R.id.content).setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        getSupportActionBar().setTitle("About plot2txt");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(getIntent().getBooleanExtra(StartProgramActivity.GOING_DOWN, false))
            overridePendingTransition(R.anim.slide1, R.anim.slide2);
        else
            overridePendingTransition(R.anim.slide3, R.anim.slide4);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.home:
                Intent intent =  new Intent(this, StartProgramActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra(StartProgramActivity.GOING_DOWN, false);
                NavUtils.navigateUpTo(this, intent);
                return true;
        }
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide3,R.anim.slide4);
    }

    public void visitOurWebsite(View view){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.plot2txt.com"));
        startActivity(browserIntent);
    }

}

