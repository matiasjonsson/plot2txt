package com.plot2txt.p2t;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileChooserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_chooser);
        getSupportActionBar().setTitle("Choose a file");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(getIntent().getBooleanExtra(StartProgramActivity.GOING_DOWN,false))
            overridePendingTransition(R.anim.slide1, R.anim.slide2);
        else
            overridePendingTransition(R.anim.slide3, R.anim.slide4);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // Respond to the action bar's Up/Home button
            case R.id.home: {
                Intent intent =  new Intent(this, StartProgramActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra(StartProgramActivity.GOING_DOWN, false);
                NavUtils.navigateUpTo(this, intent);
                return true;
            }
        }
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide3,R.anim.slide4);
    }







    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_SELECT_IMAGE = 2;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        overridePendingTransition(R.anim.slide3, R.anim.slide4);
        ImageView imageView = (ImageView)findViewById(R.id.imageView2);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            imageView.setImageURI(currentPhotoUri);
            galleryAddPic();
            changeButtons();
        }
        else if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_CANCELED)
            FileUtils.deleteQuietly(new File(currentPhotoPath));

        if (requestCode == REQUEST_SELECT_IMAGE && resultCode == RESULT_OK) {
            currentPhotoUri = data.getData();
            currentPhotoPath = getRealPathFromURI(currentPhotoUri);
            imageView.setImageURI(currentPhotoUri);
            changeButtons();
        }
    }






    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(currentPhotoUri);
        this.sendBroadcast(mediaScanIntent);
    }

    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }










    Button buttonCamera,buttonGallery,buttonChooseAnother,buttonAcceptPicture;
    ImageView preview;
    String currentPhotoPath;
    Uri currentPhotoUri;

    public void onSelectFromGalleryPressed(View view){
        Intent selectImageIntent = new Intent(Intent.ACTION_PICK);
        selectImageIntent.setType("image/*");
        startActivityForResult(selectImageIntent, REQUEST_SELECT_IMAGE);
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
    }
    public void onTakePicturePressed(View view) {
        new CameraTakePicture().execute();
    }
    public void revertButtons(View view) {
        buttonCamera.setVisibility(View.VISIBLE);
        buttonGallery.setVisibility(View.VISIBLE);
        buttonChooseAnother.setVisibility(View.GONE);
        buttonAcceptPicture.setVisibility(View.GONE);
        preview = (ImageView)findViewById(R.id.imageView2);
        preview.setImageResource(R.drawable.image);;
    }
    public void changeButtons(){
        buttonCamera = findViewById(R.id.buttonCamera);
        buttonGallery = findViewById(R.id.buttonGallery);
        buttonChooseAnother = findViewById(R.id.buttonChooseAnother);
        buttonAcceptPicture = findViewById(R.id.buttonAcceptPicture);
        System.out.println(""+buttonCamera.getVisibility()+buttonGallery.getVisibility()+buttonChooseAnother.getVisibility()+buttonAcceptPicture.getVisibility());
        buttonCamera.setVisibility(View.GONE);
        buttonGallery.setVisibility(View.GONE);
        buttonChooseAnother.setVisibility(View.VISIBLE);
        buttonAcceptPicture.setVisibility(View.VISIBLE);
        LinearLayout buttonHolder = findViewById(R.id.fileChooserButtonHolder);
        buttonHolder.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
        System.out.println(""+buttonCamera.getVisibility()+buttonGallery.getVisibility()+buttonChooseAnother.getVisibility()+buttonAcceptPicture.getVisibility());
    }
    public void acceptPicture(View view) {
        Intent intent = new Intent(this, LoadingBarActivity.class);
        intent.putExtra(StartProgramActivity.GOING_DOWN, true);
        intent.putExtra(StartProgramActivity.IMAGE_SELECTED, currentPhotoPath);
        intent.putExtra(StartProgramActivity.IMAGE_URI, currentPhotoUri.toString());
        intent.putExtra(StartProgramActivity.API_SELECTED, getIntent().getIntExtra(StartProgramActivity.API_SELECTED,0));
        startActivity(intent);
    }









    class CameraTakePicture extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params){
            if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException e) {
                        System.out.println(e.toString());
                    }
                    if (photoFile !=null) {
                        Uri photoURI = FileProvider.getUriForFile(getApplicationContext(), "com.plot2txt.p2t.fileprovider", photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                        overridePendingTransition(R.anim.slide1, R.anim.slide2);
                    }
                }
            }
            else
                Snackbar.make((CoordinatorLayout)findViewById(R.id.main_content_results),"Your device does not have a camera",Snackbar.LENGTH_LONG).show();

            return null;
        }
        public File createImageFile() throws IOException {
            // Create an image file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "plot2txt");
            if(!storageDir.exists())
                storageDir.mkdir();
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = image.getAbsolutePath();
            System.out.println(image.getAbsolutePath());
            currentPhotoUri = Uri.fromFile(image);
            return image;
        }
    }
}
